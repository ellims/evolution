﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evolution.CreatureKinds;

namespace Evolution
{
    public class Creature
    {
        #region Characteristics

        public CreatureKind Kind { get; private set; }
        public int ID { get; protected set; }
        public int Generation
        {
            get { return generation; }
            protected set
            {
                if (value > 3)
                {
                    generation = 3;
                }
                else
                    generation = value;
            }
        }
        public CreatureGender Gender { get; protected set; }        
        public int LifePoints
        {
            get { return lifePoints; }
            set
            {
                if (value > (int)Kind * Generation * 2)
                    lifePoints = (int)Kind * Generation * 2;
                else
                    lifePoints = value;
            }
        }
        public int Speed { get; private set; }
        public int AttackPower { get; private set; }
        public int Age { get; set; }

        #endregion
        #region Status

        public bool IsAlive { get; set; }
        public bool IsBusy { get; set; }
        #endregion
        #region private & service fields
        
        private int lifePoints;
        private int generation;
        private Random random = new Random();
        public MoveDirection CurrentMoveDirection;
        public Position CurrentPosition; 
        public static int PlanetSize { get; set; }

        #endregion
        #region Constructors

        public Creature(CreatureGender gender)
            : this() { Gender = gender; }
        protected Creature() 
            : this(CreatureKind.Амеба, 1) { }
        protected Creature(CreatureKind kind, int generation)
        {
            Kind = kind;
            Generation = generation;
            CurrentPosition = new Position();
            IsAlive = true;
            IsBusy = false;
            Gender = (CreatureGender)random.Next(1, 3);
            LifePoints = (int)Kind * Generation * 2;
            Speed = ((int)Kind + Generation) * 2;
            AttackPower = (int)Kind * Generation;
            Age = 0;
        }

        #endregion
        #region Methods

        public void Move()
        {
            switch (CurrentMoveDirection)
            {
                case MoveDirection.Север: CurrentPosition.Y -= 1; break;
                case MoveDirection.Юг: CurrentPosition.Y += 1; break;
                case MoveDirection.Запад: CurrentPosition.X -= 1; break;
                case MoveDirection.Восток: CurrentPosition.X += 1; break;
            }
            if (CurrentPosition.X < 0)
                CurrentPosition.X = PlanetSize + CurrentPosition.X;
            else if (CurrentPosition.X > PlanetSize)
                CurrentPosition.X -= PlanetSize;
            if (CurrentPosition.Y < 0)
                CurrentPosition.Y = PlanetSize + CurrentPosition.Y;
            else if (CurrentPosition.Y > PlanetSize)
                CurrentPosition.Y -= PlanetSize;
        }

        public void Feed()
        {
            LifePoints++;
        }

        public override string ToString()
        {
            return string.Format("{0} #{1} Gen.{2}", Kind, ID, Generation);
        }

        #endregion
    }
}
