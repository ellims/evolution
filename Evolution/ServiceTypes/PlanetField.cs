﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution
{
    public class PlanetField
    {
        public bool IsGrassHere { get; set; }
        public CreaturesList creaturesOnField;
        public PlanetField()
        {
            IsGrassHere = false;
            creaturesOnField = new CreaturesList();
        }
    }
}
