﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution
{
    public enum CreatureKind
    { Амеба = 1, Одноклеточное, Бактерия, Рыбка, Собачка, Человек, Киборг, Ктулху }

    public enum CreatureGender 
    { Самец = 1, Самка }

    public enum MoveDirection
    { Север = 1, Юг, Запад, Восток }

    public sealed class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Position() { X = 0; Y = 0; }
    }
}
