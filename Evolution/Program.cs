﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evolution.CreatureKinds;

namespace Evolution
{
    class Program
    {
        static void Main(string[] args)
        {
            Planet planet = new Planet();
           
            //Random random = new Random();
            //for (int i = 0; i < 100; i++)           //Вливание дополнительных амеб на планету
            //{
            //    planet.Creatures.Add(new Amoeba());
            //    foreach (Creature cr in planet.Creatures)
            //        cr.CurrentPosition = new Position() { X = random.Next(0, planet.Size), Y = random.Next(0, planet.Size) };
            //}

            for (int i = 0; i < 30000; i++)
            {
                Console.WriteLine("********************");
                Console.WriteLine("Цикл {0}", i);
                Console.WriteLine("********************");

                Console.WriteLine("Всего существ: {0}", planet.Creatures.Count);
                for (int c = 0; c < 8; c++)
                    Console.WriteLine(planet.PlanetCreaturesStatistics((CreatureKind)(c + 1)));
                Console.WriteLine("\nДля продолжения нажми Энтер");
                Console.ReadLine();
                planet.TimeGoes();
                Console.WriteLine("\nВсе существа использовали свои ходы. Для продолжения нажми Энтер");
                Console.ReadLine();
                if (planet.Creatures.Count == 0)
                {
                    Console.WriteLine("\nНе очень удачный виток эволюции. Все умерли.");
                    break;
                }
                Console.Clear();
            }
            Console.WriteLine("Конец циклов");
            Console.ReadLine();
        }
    }
}
