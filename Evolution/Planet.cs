﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evolution.CreatureKinds;

namespace Evolution
{
    public class Planet
    {
        private Random random = new Random();

        public CreaturesList Creatures;
        public PlanetField[][] Fields;
        public int Size { get; private set; }

        #region Constructors

        public Planet()
            : this(501)
        { }

        public Planet(int size)
        {
            Size = size;
            Creature.PlanetSize = Size - 1;
            Fields = new PlanetField[Size][];
            for (int y = 0; y < Size; y++)
            {
                Fields[y] = new PlanetField[Size];
                for (int x = 0; x < Size; x++)
                    Fields[y][x] = new PlanetField();
            }
            Creatures = new CreaturesList();
            Creatures.Add(new Creature(CreatureGender.Самец));           
            Creatures.Add(new Creature(CreatureGender.Самка));
            Creatures.Add(Creatures.CreateNewCreature(Creatures[0], Creatures[1]));
            GrowGrass();
        }

        #endregion
        #region Methods

        public void GrowGrass()
        {
            for (int i = 0; i < Size * Size / 20; i++)
                Fields[random.Next(0, Size)][random.Next(0, Size)].IsGrassHere = true;
        }

        public string PlanetCreaturesStatistics(CreatureKind kind)
        {
            string info = string.Format(kind.ToString() + ": {0}", Creatures.FindAll(cr => cr.Kind == kind).Count);
            info += string.Format("\nGen 1: {0}", Creatures.FindAll(cr => cr.Kind == kind && cr.Generation == 1).Count);
            info += string.Format("\nGen 2: {0}", Creatures.FindAll(cr => cr.Kind == kind && cr.Generation == 2).Count);
            info += string.Format("\nGen 3: {0}", Creatures.FindAll(cr => cr.Kind == kind && cr.Generation == 3).Count);
            return info;
        }

        public void TimeGoes()
        {
            CreaturesList tempList = new CreaturesList();
            Position p; 
            foreach(Creature activeCreature in Creatures)
            {
                p = activeCreature.CurrentPosition;
                if(activeCreature.IsAlive)
                {
                    activeCreature.CurrentMoveDirection = (MoveDirection)random.Next(1, 5);
                    for (int step = 0; step < activeCreature.Speed; step++)
                    {
                        if (activeCreature.IsAlive)
                        {
                            if (!activeCreature.IsBusy)
                            {
                                Fields[p.Y][p.X].creaturesOnField.Remove(activeCreature);
                                activeCreature.Move();
                                PlanetField currentField = Fields[p.Y][p.X];

                                if (currentField.creaturesOnField.Count == 0)
                                {
                                    if (currentField.IsGrassHere)
                                    {
                                        Console.WriteLine("\n{0} ест траву.", activeCreature.ToString());
                                        activeCreature.IsBusy = true;
                                        activeCreature.Feed();
                                        currentField.IsGrassHere = false;
                                    }
                                }
                                else
                                {
                                    Creature advanced;
                                    Creature backward;
                                    Creature child;
                                    foreach(Creature cr in currentField.creaturesOnField)
                                    {
                                        advanced = Creatures.GetAdvanced(activeCreature, cr);
                                        backward = Creatures.GetBackward(activeCreature, cr);

                                        if((int)advanced.Kind - (int)backward.Kind > 1)
                                        {
                                            activeCreature.IsBusy = true;
                                            Console.WriteLine("\n{0} нападает на {1} и съедает его!", advanced.ToString(), backward.ToString());
                                            advanced.LifePoints++;
                                            backward.IsAlive = false;
                                        }

                                        else if(activeCreature.Gender != cr.Gender)
                                        {
                                            activeCreature.IsBusy = true;
                                            child = Creatures.CreateNewCreature(activeCreature, cr);
                                            child.CurrentPosition = activeCreature.CurrentPosition;
                                            tempList.Add(child);
                                            Console.WriteLine("\n{0} и {1} родили на свет {2}", activeCreature.ToString(), cr.ToString(), child.ToString());
                                        }

                                        else if(activeCreature.Gender == cr.Gender)
                                        {
                                            activeCreature.IsBusy = true;
                                            Console.WriteLine("{0} дерется с {1}!", activeCreature, cr);
                                            Console.WriteLine("{0} побеждает и поправляет здоровье, поедая труп поверженного противника.", Creatures.Fighting(advanced, backward).ToString());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                activeCreature.IsBusy = false;
                            }
                        }
                        else
                            break;          
                    }
                    Fields[p.Y][p.X].creaturesOnField.Add(activeCreature);
                    activeCreature.Age++;
                }
            }
            Creatures.RemoveAll(cr => !cr.IsAlive);
            Creatures.RemoveAll(cr => cr.Age > 100);
            Creatures.AddRange(tempList);
            GrowGrass();
        }

        #endregion
    }
}


