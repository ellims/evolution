﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evolution.CreatureKinds;

namespace Evolution
{
    public class CreaturesList : List<Creature>
    {
        public Creature CreateNewCreature(Creature parent1, Creature parent2)
        {
            Creature backwardParent = this.GetBackward(parent1, parent2);
            Creature advancedParent = this.GetAdvanced(parent1, parent2);

            if (advancedParent.Kind == backwardParent.Kind && backwardParent.Generation == 3)
            {
                switch(backwardParent.Kind)
                {
                    case CreatureKind.Амеба: return new Unicellular();
                    case CreatureKind.Одноклеточное: return new Bacterium();
                    case CreatureKind.Бактерия: return new Fish();
                    case CreatureKind.Рыбка: return new Dog();
                    case CreatureKind.Собачка: return new Human();
                    case CreatureKind.Человек: return new Cyborg();
                    case CreatureKind.Киборг: return new Khtulkhu();
                    case CreatureKind.Ктулху: return new Khtulkhu();
                    default: return new Amoeba();
                }
            }
            else
            {
                switch (backwardParent.Kind)
                {
                    case CreatureKind.Амеба: return new Amoeba(backwardParent.Generation + 1);
                    case CreatureKind.Одноклеточное: return new Unicellular(backwardParent.Generation + 1);
                    case CreatureKind.Бактерия: return new Bacterium(backwardParent.Generation + 1);
                    case CreatureKind.Рыбка: return new Fish(backwardParent.Generation + 1);
                    case CreatureKind.Собачка: return new Dog(backwardParent.Generation + 1);
                    case CreatureKind.Человек: return new Human(backwardParent.Generation + 1);
                    case CreatureKind.Киборг: return new Cyborg(backwardParent.Generation + 1);
                    case CreatureKind.Ктулху: return new Khtulkhu(backwardParent.Generation + 1);
                    default: return new Amoeba();
                }
            }
        }

        public Creature Fighting(Creature attacker, Creature defender)
        {
            defender.LifePoints -= attacker.AttackPower;
            if (defender.LifePoints <= 0)
            {
                defender.IsAlive = false;               
                attacker.LifePoints += 1;
                return attacker;
            }
            else
               return Fighting(defender, attacker);
        }

        public Creature GetBackward(Creature cr1, Creature cr2)
        {
            if (cr1.Kind > cr2.Kind)
                return cr2;
            else if (cr1.Kind < cr2.Kind)
                return cr1;
            else if (cr1.Generation > cr2.Generation)
                return cr2;
            else if (cr1.Generation < cr2.Generation)
                return cr1;
            else
                return cr1;
        }

        public Creature GetAdvanced(Creature cr1, Creature cr2)
        {
            if (cr1.Kind > cr2.Kind)
                return cr1;
            else if (cr1.Kind < cr2.Kind)
                return cr2;
            else if (cr1.Generation > cr2.Generation)
                return cr1;
            else if (cr1.Generation < cr2.Generation)
                return cr2;
            else
                return cr1;
        }
    }
}
