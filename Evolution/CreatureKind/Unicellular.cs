﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Unicellular : Creature
    {
        public static int Count { get; private set; }
        
        public Unicellular()
            : this(CreatureKind.Одноклеточное, 1) { }

        public Unicellular(int generation)
            : this(CreatureKind.Одноклеточное, generation) { }

        private Unicellular(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
