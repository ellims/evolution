﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Khtulkhu : Creature
    {
        public static int Count { get; private set; }
        
        public Khtulkhu()
            : this(CreatureKind.Ктулху, 1) { }

        public Khtulkhu(int generation)
            : this(CreatureKind.Ктулху, generation) { }

        public Khtulkhu(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
