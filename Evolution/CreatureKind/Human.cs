﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Human : Creature
    {
        public static int Count { get; private set; }
        
        public Human()
            : this(CreatureKind.Человек, 1) { }

        public Human(int generation)
            : this(CreatureKind.Человек, generation) { }

        private Human(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
