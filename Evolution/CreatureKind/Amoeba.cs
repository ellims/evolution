﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Amoeba : Creature
    {
        public static int Count { get; private set; }
        
        public Amoeba()
            : this(CreatureKind.Амеба, 1) { }
        
        public Amoeba(int generation)
            : this(CreatureKind.Амеба, generation) { }

        private Amoeba(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
