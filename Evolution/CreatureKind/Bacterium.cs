﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public sealed class Bacterium : Creature
    {
        public static int Count { get; private set; }
        
        public Bacterium()
            : this(CreatureKind.Бактерия, 1) { }
        
        public Bacterium(int generation)
            : this(CreatureKind.Бактерия, generation) { }

        private Bacterium(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
