﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Dog : Creature
    {
        public static int Count { get; private set; }

        public Dog()
            : this(CreatureKind.Собачка, 1) { }
        
        public Dog(int generation)
            : this(CreatureKind.Собачка, generation) { }

        private Dog(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
