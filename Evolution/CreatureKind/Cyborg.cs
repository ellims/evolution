﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Cyborg : Creature
    {
        public static int Count { get; private set; }
        
        public Cyborg()
            : this(CreatureKind.Киборг, 1) { }
        
        public Cyborg(int generation)
            : this(CreatureKind.Киборг, generation) { }

        private Cyborg(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
