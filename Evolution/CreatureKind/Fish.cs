﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.CreatureKinds
{
    public class Fish : Creature
    {
        public static int Count { get; private set; }
        
        public Fish()
            : this(CreatureKind.Рыбка, 1) { }

        public Fish(int generation)
            : this(CreatureKind.Рыбка, generation) { }

        private Fish(CreatureKind kind, int generation)
            : base(kind, generation)
        { ID = ++Count; }
    }
}
